library (stats)
library (plyr)
library(corrplot)

# Traemos la matriz con todos los datos
features<-read.csv("C:\\Users\\e001255\\Desktop\\Big Data\\prueba_modelo\\input\\final_dataframe_new.csv",header=TRUE,dec=".",sep=";", colClasses=c("id_sscc"="character"))

# eliminamos todas las variables que no queramos
colnames(features)
drops <- c("CUSEC")
features<-features[ , !(names(features) %in% drops)]

# hay que comprobar que las variables son num�ricas para las regresiones
str(features, list.len=ncol(features))
f <- sapply(features, is.factor)
colnames(features[,f])

#ponemos las variables que estan como factor en string
features$X2km_SALUD<-as.character(features$X2km_SALUD)
features$X2km_ZONAS_VERDES<-as.character(features$X2km_ZONAS_VERDES)
features$X2km_BOMBEROS<-as.character(features$X2km_BOMBEROS)
features$X2km_POLICIA<-as.character(features$X2km_POLICIA)

#cambiamos las variables dummy string a dummy numerical 0,1
features [features == "True"] <- 1
features [features == "False"] <- 0

#ponemos las variables que estan como factor en string
features$X2km_SALUD<-as.integer(features$X2km_SALUD)
features$X2km_ZONAS_VERDES<-as.integer(features$X2km_ZONAS_VERDES)
features$X2km_BOMBEROS<-as.integer(features$X2km_BOMBEROS)
features$X2km_POLICIA<-as.integer(features$X2km_POLICIA)

#ordenamos la matriz tal que quede provincia, id_sscc y el resto
features <- features[ ,c(59,1,58,2:57,60:99)]

#calculamos la matriz de correlaci�n de todas las variables
dat <- subset(features, select= c(-1,-2))
lapply(dat, function(x) any(is.na(x)))
dat[is.na(dat)]<-0

M <- cor(dat)
corrplot(M, method="circle")

corr_all<-cor(dat)
write.csv(corr_all, "C:\\Users\\e001255\\Desktop\\Big Data\\prueba_modelo\\output\\correlation.csv",row.names=TRUE)

#analizamos las componentes principales de todos los datos
zdat<-scale(dat)
pc_dat=prcomp(zdat)
plot(pc_dat, type = "l")
summary(pc_dat)

#vamos a dividir la muestra por subgrupos tem�ticos

zlocation<-zdat[,27:57]
zine<-zdat[,2:26]
ztargetes<-zdat[,58:97]

L <- cor(zlocation)
corrplot(L, method="circle", order="hclust", addrect=3)

I <- cor(zine)
corrplot(I, method="circle", order="hclust", addrect=3)

Ta <- cor(ztargetes)
corrplot(Ta, method="circle", order="hclust", addrect=3)

#sacamos componentes principales por subgrupos

#Variables de localizaci�n
#Dejamos solo las variables que est�n agregadas
zlocation_clean<-zlocation[,19:27]
#si adem�s excluimos zonas verdes, polic�a y bomberos....
zlocation_clean_ex<- subset(zlocation_clean, select= c(-6,-8, -9)) 

pc_location=prcomp(zlocation_clean_ex)
plot(pc_location, type = "l")
summary(pc_location)
pc_location
pc_location$x[,1:2] #con esto seleccionamos las 2 CPs que suponen 80% de la varianza
PC_LOC<-data.frame(pc_location$x[,1:2])

#Variables de tpvs

#si adem�s excluimos autos, bank, bbva, contents, hotelservices,
#hyper, leissure, propertyservices, sports and toys y travel
ztargetes_clean<- subset(ztargetes, select= c(-2,-14,-21,-33,-39,-40))

#dividimos la muestra entre niveles e importes
ztargetes_clean_levels<-subset(ztargetes_clean, select= c(1:17))
ztargetes_clean_imp<-subset(ztargetes_clean, select= c(18:34))

pc_targetes=prcomp(ztargetes_clean)
plot(pc_targetes, type = "l")
#options(digits=10)
summary(pc_targetes)
print(pc_targetes, digits=3)
pc_targetes$x[,1:2]
PC_TARGETES<-data.frame(pc_targetes$x[,1:2])

#Variables ine

zine_all<-zine
#Variables sociales
zine_soc<- zine_all[,1:8] 
#Variables sociales sin % hombres, ni edades ni estado civil ni nacionalidad
zine_soc_red<- subset(zine_soc, select= c(-1,-2,-3,-4))
#Variables estructura viviendas
zine_viv<- zine_all[,9:25] 
zine_viv_tipo<-zine_viv[,1:6]
zine_viv_size<-zine_viv[,7:14]
#zine_viv_size2<-zine_viv[,12:14]


pc_soc=prcomp(zine_soc_red)
plot(pc_soc, type = "l")
summary(pc_soc)
pc_soc$x[,1:2] #con esto seleccionamos las 2 CPs que suponen 80% de la varianza
PC_SOC<-data.frame(pc_soc$x[,1:2])

pc_size=prcomp(zine_viv_size)
plot(pc_size, type = "l")
summary(pc_size)
pc_size$x[,1:4] #con esto seleccionamos las 4 CPs que suponen 70% de la varianza
PC_SIZE<-data.frame(pc_size$x[,1:4])

#renombramos las columnas de los DF de componetes principales
PC_LOC<-rename(PC_LOC, c("PC1"="PC1_LOC", "PC2"="PC2_LOC"))
PC_SOC<-rename(PC_SOC, c("PC1"="PC1_SOC", "PC2"="PC2_SOC"))
PC_SIZE<-rename(PC_SIZE, c("PC1"="PC1_SIZE", "PC2"="PC2_SIZE", "PC3"="PC3_SIZE", "PC4"="PC4_SIZE"))
PC_TARGETES<-rename(PC_TARGETES, c("PC1"="PC1_TARGETES", "PC2"="PC2_TARGETES"))

#Generamos la matriz de componentes principales

CPS<-cbind.data.frame(features$id_sscc)
CPS<-cbind.data.frame(CPS,PC_LOC, PC_SOC, PC_SIZE, PC_TARGETES)

write.csv(CPS, "C:\\Users\\e001255\\Desktop\\Big Data\\prueba_modelo\\output\\PCA.csv", row.names=FALSE)
