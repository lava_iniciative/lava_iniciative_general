rm(list=ls())

library (plotmo)
library(ggplot2)
library(RColorBrewer)
library(classInt)
library(dplyr)
library(Hmisc)
library(lmtest)
library(tseries)
library(spdep)
library(maptools)
library(rgdal)

#traemos el shapefile ine_madiva
data.shape<-readOGR(dsn="C://Users//e001255//Desktop//Big Data//prueba_modelo//input//shp_ine_madiva",layer="valoracion_madiva_seccion_censal")

#transformamos la projección a WGS84 (EPSG: 4326)
data.shape<-spTransform(data.shape, CRS("+init=epsg:4326"))

#traemos la matriz con todas las caracterasticas de las secciones censales
features<-read.csv("C:\\Users\\e001255\\Desktop\\Big Data\\prueba_modelo\\input\\final_dataframe.csv",header=TRUE, dec=",",sep=";", colClasses=c("id_sscc"="character"))

#traemos las variables PCA
pca<-read.csv("C:\\Users\\e001255\\Desktop\\Big Data\\prueba_modelo\\output\\PCA.csv", header=TRUE ,sep=";", dec=".", colClasses=c("sscc"="character")) 

#creamos un subset para Madrid y para Barcelona
mad <- subset(data.shape, CUMUN == 28079, select= c(-1,-2))
mad_features<- features[ which(features$provincia=='28'), ]

#ponemos las columnas de sscc en string para poder cruzarlo
mad$PRMUDISEC <- as.character(mad$PRMUDISEC)
length(unique(mad@data$PRMUDISEC)) # comprobamos que no se repiten sscc
sum(is.na(mad@data$euros_m2_r)) #comprobamos cuantos precios son NA

##################################################
# FUNCTION TO REMOVE NA's IN sp DataFrame OBJECT #
#   x           sp spatial DataFrame object      #
#   margin      Remove rows (1) or columns (2)   #
##################################################

sp.na.omit <- function(x, margin=1) {
  if (!inherits(x, "SpatialPointsDataFrame") & !inherits(x, "SpatialPolygonsDataFrame")) 
    stop("MUST BE sp SpatialPointsDataFrame OR SpatialPolygonsDataFrame CLASS OBJECT") 
  na.index <- unique(as.data.frame(which(is.na(x@data$euros_m2_r),arr.ind=TRUE))[,margin])
  if(margin == 1) {  
    cat("DELETING ROWS: ", na.index, "\n") 
    return( x[-na.index,]  ) 
  }
  if(margin == 2) {  
    cat("DELETING COLUMNS: ", na.index, "\n") 
    return( x[,-na.index]  ) 
  }
}

mad_clean <- sp.na.omit(mad) #ya tenemos generada una matriz espacial en la que no hay precios

#hacemos un merge de mad_clean y features
mad_all<-merge(mad_clean,mad_features,by.x="PRMUDISEC", by.y="id_sscc", all.x = TRUE)
mad_all<-merge(mad_all,pca,by.x="PRMUDISEC", by.y="sscc", all.x = TRUE)

colnames(mad_all@data) #para ver el nombre de las columnas que tenemos

#creamos una submuestra con varios distritos censales

#mad_center <- mad_all[ which(mad_all$CDIS=='03'),]
#mad_center <- mad_all[ which(mad_all$CDIS=='01' | mad_all$CDIS=='02' | mad_all$CDIS=='03' | mad_all$CDIS=='04' | mad_all$CDIS=='05'),]


#creamos la matriz de contiguidad estandarizada

# Contiguity-based neighbours by Queen criteria and minimum threshold distance
# We create 2 weighted spatial matrices. 1 with queen, 2. with minimum threshold distance
# 3. One that removes 0 neighbours at min threshold distance with queen neighbours

contnb_q<-poly2nb(mad_all,queen = T) #queen=false would be rook method
contnb_dist<-dnearneigh(coordinates(mad_all),0,1,longlat = TRUE)

zeroneighbours<-which(card(contnb_dist)==0)
contnb_mix<-contnb_dist

for(x in zeroneighbours){
  contnb_mix[x]<-contnb_q[x]
  print(contnb_q[x])
}

which(card(contnb_mix)==0)

#getting the weight and normalizad weight matriz
w_q<-nb2listw(contnb_q, glist = NULL)
ws_q<-nb2listw(contnb_q, glist = NULL, style = "W" )
w_dist<-nb2listw(contnb_dist, glist = NULL, zero.policy=TRUE)
ws_dist<-nb2listw(contnb_dist, glist = NULL,zero.policy=TRUE, style = "W" )
w_mix<-nb2listw(contnb_mix, glist = NULL)
ws_mix<-nb2listw(contnb_mix, glist = NULL, style = "W" )

summary(ws_q)
summary(ws_dist)
summary(ws_mix)

#graficamos las relaciones de los centroides con sus vecinos

plot(mad_all, border="grey")
plot(contnb_q, coordinates(mad_all), add=TRUE, col="darkred")

plot(mad_all, border="grey")
plot(contnb_dist, coordinates(mad_all), add=TRUE, col="darkred")

plot(mad_all, border="grey")
plot(contnb_mix, coordinates(mad_all), add=TRUE, col="darkred")


##################################
#ANALISIS DE LA RELACION ESPACIAL#
##################################

#estudiamos la correlación de Moran para ver si hay correlación espacial e la variable precios
moran_q<-moran.test(mad_all@data$precios,ws_q) #Null is autoco = 0
moran_q
moran_dist<-moran.test(mad_all@data$precios,ws_dist) #Null is autoco = 0
moran_dist
moran_mix<-moran.test(mad_all@data$precios,ws_mix) #Null is autoco = 0
moran_mix

#gráficos de dispersión de Moran
moran.plot(mad_all@data$precios,ws_q, pch=20)
moran.plot(mad_all@data$precios,ws_mix, pch=20)

#estudiamos la relación espacial local con modelos LISA, 
#utilizado para la identificación de clusters, a partir
#de una evaluación por localidad del indice de Moran

#realizamos un mapa de cuartiles de los precios para hacer estratificación
sep<-round(quantile(mad_all@data$precios, probs = seq(0,1,0.25)), digits = 2)
#especificamos la paleta de colores y graficamos
colores<-brewer.pal(4,"Blues")
plot(mad_all, col=colores[findInterval(mad_all@data$precios,sep, all.inside = TRUE)],
     axes=F)

#Evaluamos estadísticamente la asociación espacial con análisis LISA
#Valores de referencia z de la distribución t

z<-c(1.65,1.96)
zc<-c(2.8284,3.0471)

f.Ti<-localmoran(mad_all@data$precios,ws_q)
zTi<-f.Ti[,] #Asignacion de la distribucion z del Ti
mx<-max(zTi)
mn<-min(zTi)

pal<-c("white", "green", "darkgreen")
z3.Ti<- classIntervals(zTi, n=3, style="fixed", fixedBreaks=c(mn,z,mx))
cols.Ti<- findColours(z3.Ti,pal)
plot(mad_all, cols=cols.Ti)


####################
#MODELOS ESPACIALES#
####################

#primero hacemos un modelo lineal, y analizamos si tueviera correlación espacial

lm.mad_vab=lm(precios ~ CBD_min + X2km_TURISMO
          + X2km_CULTURA + sin_estudios_perc + 
            estudios_tercer_grado_perc, data=mad_all)
summary(lm.mad_vab)

#regresión de las componentes principales
lm.mad_pc=lm(precios ~ CBD_min + PC1_LOC
              + PC2_LOC + PC1_SOC + PC2_SOC + PC1_SIZE
                + PC2_SIZE + PC3_SIZE + PC4_SIZE
             + PC1_TARGETES+ PC2_TARGETES, data=mad_all)
summary(lm.mad_pc)


#analizamos los tests de homocedasticidad y de normalidad

bptest(lm.mad_vab) # null is homos
bptest(lm.mad_pc)
jarque.bera.test(lm.mad_vab$residuals) #null is normality
jarque.bera.test(lm.mad_pc$residuals)

#analizamos la corelación entre los residuos y la matriz de contiguidad
moran_q_vab<-lm.morantest(lm.mad_vab,ws_q) #Null is autoco = 0
moran_q_vab
moran_mix_vab<-lm.morantest(lm.mad_vab,ws_mix) #Null is autoco = 0
moran_mix_vab

moran_q_pc<-lm.morantest(lm.mad_pc,ws_q) #Null is autoco = 0
moran_q_pc
moran_mix_pc<-lm.morantest(lm.mad_pc,ws_mix) #Null is autoco = 0
moran_mix_pc

#Lagrange Multiplier Test Statistics for Spatial Autocorrelation
lagrange_q_vab<-lm.LMtests(lm.mad_vab,ws_q, test=c("LMerr","RLMerr","LMlag","RLMlag","SARMA"))
lagrange_q_vab
lagrange_mix_vab<-lm.LMtests(lm.mad_vab,ws_mix, test=c("LMerr","RLMerr","LMlag","RLMlag","SARMA"))
lagrange_mix_vab

lagrange_q_pc<-lm.LMtests(lm.mad_pc,ws_q, test=c("LMerr","RLMerr","LMlag","RLMlag","SARMA"))
lagrange_q_pc
lagrange_mix_pc<-lm.LMtests(lm.mad_pc,ws_mix, test=c("LMerr","RLMerr","LMlag","RLMlag","SARMA"))
lagrange_mix_pc

#Purely autoregressive model
model0<-spautolm(precios ~ 1, data=mad_all,listw=ws_q)
summary(model0)

#sapatial error model
model_error_vab_mvs<-errorsarlm(precios ~ CBD_min + X2km_TURISMO
                  + X2km_CULTURA + sin_estudios_perc + 
                     estudios_tercer_grado_perc, 
                     data=mad_all,listw=ws_q) #mvs

summary(model_error_vab_mvs)

model_error_pc_mvs<-errorsarlm(precios ~ CBD_min + PC1_LOC + PC2_LOC
                               + PC1_SOC + PC2_SOC + PC1_SIZE 
                               +  PC2_SIZE+ PC3_SIZE+ PC4_SIZE
                               + PC1_TARGETES + PC2_TARGETES , 
                               data=mad_all,listw=ws_q) #GLS
summary(model_error_pc_mvs)

model_error_vab_gls<-GMerrorsar(precios ~ CBD_min + X2km_TURISMO
                   + X2km_CULTURA + sin_estudios_perc + 
                     estudios_tercer_grado_perc, 
                   data=mad_all,listw=ws_mix) #GLS
summary(model_error_vab_gls)

model_error_pc_gls<-GMerrorsar(precios ~ CBD_min + PC1_LOC + PC2_LOC
                            + PC1_SOC + PC2_SOC + PC1_SIZE 
                            +  PC2_SIZE+ PC3_SIZE+ PC4_SIZE
                            + PC1_TARGETES + PC2_TARGETES , 
                            data=mad_all,listw=ws_mix) #GLS
summary(model_error_pc_gls)


#Spatial Lag Model (SLM)
model_lag_vab_mvs <- lagsarlm(precios ~ CBD_min + X2km_TURISMO
                   + X2km_CULTURA + sin_estudios_perc + 
                     estudios_tercer_grado_perc, 
                    data=mad_all,listw=ws_q) #mvs
summary(model_lag_vab_mvs)

model_lag_vab_2gls <- stsls(precios ~ CBD_min + X2km_TURISMO
                + X2km_CULTURA + sin_estudios_perc + 
                  estudios_tercer_grado_perc,
                   data=mad_all,listw=ws_q) #2GLS

summary(model_lag_vab_2gls)


model_lag_pc_mvs<-lagsarlm(precios ~ CBD_min + PC1_LOC + PC2_LOC
                               + PC1_SOC + PC2_SOC + PC1_SIZE 
                               +  PC2_SIZE+ PC3_SIZE+ PC4_SIZE
                               + PC1_TARGETES + PC2_TARGETES , 
                               data=mad_all,listw=ws_q) #MVS
summary(model_lag_pc_mvs)

model_lag_pc_2gls<-stsls(precios ~ CBD_min + PC1_LOC + PC2_LOC
                           + PC1_SOC + PC2_SOC + PC1_SIZE 
                           +  PC2_SIZE+ PC3_SIZE+ PC4_SIZE
                           + PC1_TARGETES + PC2_TARGETES , 
                           data=mad_all,listw=ws_q) #2GLS
summary(model_lag_pc_2gls)


#SARAR 

model_sarar_vab_mvs <- sacsarlm(precios ~ CBD_min + X2km_TURISMO
                   + X2km_CULTURA + sin_estudios_perc + 
                     estudios_tercer_grado_perc, 
                   data=mad_all,listw=ws_q)  #mvs
summary(model_sarar_vab_mvs)


model_sarar_vab_gs2sls <- gstsls(precios ~ CBD_min + X2km_TURISMO
                   + X2km_CULTURA + sin_estudios_perc + 
                     estudios_tercer_grado_perc, 
                   data=mad_all,listw=ws_q)  #GS2SLS
summary(model_sarar_vab_gs2sls)

model_sarar_pc_mvs<-sacsarlm(precios ~ CBD_min + PC1_LOC + PC2_LOC
                           + PC1_SOC + PC2_SOC + PC1_SIZE 
                           +  PC2_SIZE+ PC3_SIZE+ PC4_SIZE
                           + PC1_TARGETES + PC2_TARGETES , 
                           data=mad_all,listw=ws_q) #MVS
summary(model_sarar_pc_mvs)

model_sarar_pc_gs2sls<-gstsls(precios ~ CBD_min + PC1_LOC + PC2_LOC
                             + PC1_SOC + PC2_SOC + PC1_SIZE 
                             +  PC2_SIZE+ PC3_SIZE+ PC4_SIZE
                             + PC1_TARGETES + PC2_TARGETES , 
                             data=mad_all,listw=ws_q) #GS2SLS
summary(model_sarar_pc_gs2sls)

# Durbin with Spatial Lag

model7

#by nearest neighbours
#contnb <-  knn2nb(knearneigh(coordinates(mad_all), k=5))
plot(mad_all, border="grey")
plot(contnb, coordinates(mad_all), add=TRUE)
#title(main="K nearest neighbours, k = 4")

